﻿using GdPicture14;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace Unir
{
    public partial class Form1 : Form
    {
        private string TIFF_CODEC = "image/tiff";
        private long ENCODING_SCHEME = (long)EncoderValue.CompressionCCITT4;
        public string SelectedPages { get; set; }
        public int NumberOfPages { get; set; }
        public string FilePath { get; set; }
        public bool newfile = true;
        public bool newdocument = true;
        Image Document;
        EncoderParameters encoderParams;
        public int SelectedPageCount { get { return Pages.Length; } }
        //First method to call in order to load the file and get its contents and pages

        public Form1()
        {
            InitializeComponent();
        }

        private static ImageCodecInfo GetEncoderInfo(String mimeType)
        {
            int j;
            ImageCodecInfo[] encoders;
            encoders = ImageCodecInfo.GetImageEncoders();
            for (j = 0; j < encoders.Length; ++j)
            {
                if (encoders[j].MimeType == mimeType)
                    return encoders[j];
            }
            return null;
        }


        private void button3_Click(object sender, EventArgs e)
        {
            string file = txt_1.Text;
            string sOutFilePath = txt_3.Text;
       
            Bitmap bitmap = (Bitmap)Image.FromFile(file);
            MemoryStream byteStream = new MemoryStream();
            bitmap.Save(byteStream, ImageFormat.Tiff);


            Image tiff = Image.FromStream(byteStream);
            //Preparar codificadores:

            ImageCodecInfo encoderInfo = GetEncoderInfo("image/tiff");

            EncoderParameters encoderParams = new EncoderParameters(2);
            EncoderParameter parameter = new EncoderParameter( System.Drawing.Imaging.Encoder.Compression, (long)EncoderValue.CompressionCCITT4);
            encoderParams.Param[0] = parameter;
            parameter = new EncoderParameter(System.Drawing.Imaging.Encoder.SaveFlag,  (long)EncoderValue.MultiFrame);
            encoderParams.Param[1] = parameter;
            //Guardar en archivo:

            tiff.Save(sOutFilePath, encoderInfo, encoderParams);
            //Para las páginas siguientes, prepare los codificadores:
            Bitmap U = (Bitmap)Image.FromFile(txt_2.Text);
            EncoderParameters EncoderParams = new EncoderParameters(2);
            EncoderParameter SaveEncodeParam = new EncoderParameter( System.Drawing.Imaging.Encoder.SaveFlag, (long)EncoderValue.FrameDimensionPage);
            EncoderParameter CompressionEncodeParam = new EncoderParameter(  System.Drawing.Imaging.Encoder.Compression, (long)EncoderValue.CompressionCCITT4);
            EncoderParams.Param[0] = CompressionEncodeParam;
            EncoderParams.Param[1] = SaveEncodeParam;
            tiff.SaveAdd(U, EncoderParams);
            // Finalmente purgue el archivo:

            //encoderParameters.Param[0] = new EncoderParameter(encoder, (long)EncoderValue.Flush);
            //firstImage.SaveAdd(encoderParameters);
            ///7
            SaveEncodeParam = new EncoderParameter(  System.Drawing.Imaging.Encoder.SaveFlag, (long)EncoderValue.Flush);
            EncoderParams = new EncoderParameters(1);
            EncoderParams.Param[0] = SaveEncodeParam;
            tiff.SaveAdd(EncoderParams);
         



        }

        private void button1_Click(object sender, EventArgs e)
        {
            GdPictureImaging oGdPictureImaging = new GdPictureImaging();
            int ImageID = oGdPictureImaging.CreateGdPictureImageFromFile(txt_1.Text);
            if (oGdPictureImaging.GetStat() == GdPictureStatus.OK)
            {
                if ((oGdPictureImaging.TiffAppendPageFromFile(ImageID, txt_2.Text) != GdPictureStatus.OK) ||
                     (oGdPictureImaging.TiffSaveMultiPageToFile(ImageID, txt_3.Text, TiffCompression.TiffCompressionAUTO) != GdPictureStatus.OK))
                {
                    MessageBox.Show("Error: " + oGdPictureImaging.GetStat().ToString(), "Multipage TIFF Example", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                oGdPictureImaging.ReleaseGdPictureImage(ImageID);
            }
            else
            {

                MessageBox.Show("Error: " + oGdPictureImaging.GetStat().ToString(), "Multipage TIFF Example", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            oGdPictureImaging.Dispose();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            GdPictureImaging oGdPictureImaging = new GdPictureImaging();
            //Create a handle to a multipage tiff file with the first page image1.tif.
            int TiffID = oGdPictureImaging.CreateGdPictureImageFromFile(txt_2.Text);
            if (oGdPictureImaging.GetStat() == GdPictureStatus.OK)
            {
                GdPictureStatus status = oGdPictureImaging.TiffSaveAsMultiPageFile(TiffID, txt_1.Text, TiffCompression.TiffCompressionAUTO);
                if (status == GdPictureStatus.OK)
                {
                    //Add the second page to the multipage tiff file from image2.tif.
                    int ImageID = oGdPictureImaging.CreateGdPictureImageFromFile(txt_3.Text);
                    if (oGdPictureImaging.GetStat() == GdPictureStatus.OK)
                    {
                        status = oGdPictureImaging.TiffAddToMultiPageFile(TiffID, ImageID);
                        if (status == GdPictureStatus.OK)
                            MessageBox.Show("Done!", "Multipage TIFF Example", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        else
                            MessageBox.Show("Error: " + status.ToString(), "Multipage TIFF Example", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    else
                        MessageBox.Show("Error: " + oGdPictureImaging.GetStat().ToString(), "Multipage TIFF Example", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    //Close the tiff file.
                    oGdPictureImaging.TiffCloseMultiPageFile(TiffID);
                    //Release the image resource from the memory.
                    oGdPictureImaging.ReleaseGdPictureImage(ImageID);
                }
                else
                    MessageBox.Show("Error: " + status.ToString(), "Multipage TIFF Example", MessageBoxButtons.OK, MessageBoxIcon.Error);
                oGdPictureImaging.ReleaseGdPictureImage(TiffID);
            }
            else
                MessageBox.Show("Error: " + oGdPictureImaging.GetStat().ToString(), "Multipage TIFF Example", MessageBoxButtons.OK, MessageBoxIcon.Error);
            oGdPictureImaging.Dispose();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Merge(txt_2.Text, txt_1.Text);
        }

        public void Merge(string MergeFileName, string source)
        {
            //Set its Image Format as TIFF
            ImageCodecInfo codecInfo = GetCodecInfo(TIFF_CODEC);
            //Load the Document
            FileStream fs = new FileStream(source, FileMode.OpenOrCreate);
            Image image = Image.FromStream(fs);
            //Get file frame/pages
            FrameDimension frameDim = new FrameDimension(image.FrameDimensionsList[0]);
            if (newdocument == true)
            {
                //Set its Image Type
                encoderParams = new EncoderParameters(2);
                encoderParams.Param[0] = new
                  EncoderParameter(System.Drawing.Imaging.Encoder.SaveFlag,
                  (long)EncoderValue.MultiFrame);
                encoderParams.Param[1] = new
                  EncoderParameter(System.Drawing.Imaging.Encoder.Quality, ENCODING_SCHEME);
                //Check if selected pages is null or 0 value
                if (SelectedPages != null)
                {
                    //for each frame/pages create the new document
                    for (int i = 0; i < image.GetFrameCount(frameDim); i++)
                    {
                        //check whether selected pages is not greater than the file pages
                        if (Pages.Length >= (i + 1))
                        {
                            //Selected image frame
                            image.SelectActiveFrame(frameDim, Pages[i]);
                            //check whether file is new document
                            if (newfile == true)
                            {
                                //create new filename
                                Document = image;
                                //save
                                Document.Save(MergeFileName, codecInfo, encoderParams);
                                encoderParams.Param[0] =
                                new EncoderParameter(System.Drawing.Imaging.Encoder.SaveFlag,
                                (long)EncoderValue.FrameDimensionPage);
                                newfile = false;
                                newdocument = false;
                            }
                            else
                            {
                                //append the document depending on the selected frame from the original image
                                Document.SaveAdd(image, encoderParams);
                            }
                        }
                    }
                    fs.Close();
                }
            }
            else
            {
                //Check if selected pages is null or 0 value
                if (SelectedPages != null)
                {
                    //for each frame/pages create the new document
                    for (int i = 0; i < image.GetFrameCount(frameDim); i++)
                    {
                        //check whether selected pages is not greater than the file pages
                        if (Pages.Length >= (i + 1))
                        {
                            //Selected image frame
                            image.SelectActiveFrame(frameDim, Pages[i]);
                            //check whether file is new document
                            //append the document depending on the selected frame from the original image
                            Document.SaveAdd(image, encoderParams);
                        }
                    }
                    fs.Close();
                }
            }
        }


        public int[] Pages
        {
            get
            {
                ArrayList ps = new ArrayList();
                string[] ss = SelectedPages.Split(new char[] { ',', ' ', ';' });
                foreach (string s in ss)
                    if (Regex.IsMatch(s, @"\d+-\d+"))
                    {
                        int start = int.Parse(s.Split(new char[] { '-' })[0]);
                        int end = int.Parse(s.Split(new char[] { '-' })[1]);
                        if (start > end)
                            return new int[] { 0 };
                        while (start <= end)
                        {
                            ps.Add(start - 1);
                            start++;
                        }
                    }
                    else
                    {
                        int i;
                        int.TryParse(s, out i);
                        if (i > 0)
                            ps.Add(int.Parse(s) - 1);
                    }
                return ps.ToArray(typeof(int)) as int[];
            }
        }
        private ImageCodecInfo GetCodecInfo(string codec)
        {
            ImageCodecInfo codecInfo = null;
            foreach (ImageCodecInfo info in ImageCodecInfo.GetImageEncoders())
            {
                if (info.MimeType == codec)
                {
                    codecInfo = info;
                    break;
                }
            }
            return codecInfo;
        }
   






}
}
